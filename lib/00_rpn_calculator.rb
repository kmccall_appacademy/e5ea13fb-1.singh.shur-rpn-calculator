require 'byebug'

class RPNCalculator

  def initialize
    @stack = []
  end

  def push(n)
    @stack.push(n)
  end

  def plus
    @stack.push(:+)
    resolve
  end

  def minus
    @stack.push(:-)
    resolve
  end

  def divide
    @stack.push(:/)
    resolve(true)
  end

  def times
    @stack.push(:*)
    resolve(true)
  end

  def value
    @stack[-1]
  end

  def resolve(make_float = false)
    op = @stack.pop
    right = @stack.pop
    left = @stack.pop
    raise 'calculator is empty' if op.nil? || right.nil? || left.nil?
    if make_float
      right += 0.0
      left += 0.0
    end
    @stack.push(left.send(op, right))
  end

  def tokens(str)
    numbers = %w[0 1 2 3 4 5 6 7 8 9]
    ops = %w[+ - / *]
    temp_stack = []
    str.gsub(/\s/, '').chars.each do |c|
      if numbers.include?(c)
        temp_stack << c.to_i
      elsif ops.include?(c)
        temp_stack << c.to_sym
      end
    end
    temp_stack
  end

  def evaluate(str)

    tokens(str).each do |t|
      if t.is_a?(Integer)
        push(t)
      elsif t == :+
        plus
      elsif t == :-
        minus
      elsif t == :/
        divide
      elsif t == :*
        times
      end
    end
    @stack.last
  end
end
